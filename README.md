# STUDENTUML

StudentUML is a UML diagram tool for students. The main characteristics of the tool are:

* simplicity

* consistency checking

The tool has been developed by CITY college students and academic staff.
